#include "gtest/gtest.h"
#include "src/Array2D.h"

TEST(Array2DTest, TestArrayAccess) {
	const unsigned height = 2;
	const unsigned width = 3;
	Array2D<int> two_dimensional_array(height, width);

	two_dimensional_array(0, 0) = 0;
	two_dimensional_array(1, 0) = 1;

	two_dimensional_array(0, 1) = 22;
	two_dimensional_array(1, 1) = 23;

	two_dimensional_array(0, 2) = 33;
	two_dimensional_array(1, 2) = 34;

	EXPECT_EQ(0, two_dimensional_array(0, 0));
	EXPECT_EQ(1, two_dimensional_array(1, 0));
	EXPECT_EQ(22, two_dimensional_array(0, 1));
	EXPECT_EQ(23, two_dimensional_array(1, 1));
	EXPECT_EQ(33, two_dimensional_array(0, 2));
	EXPECT_EQ(34, two_dimensional_array(1, 2));
}