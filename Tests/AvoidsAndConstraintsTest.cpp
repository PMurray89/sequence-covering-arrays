#include "gtest/gtest.h"
#include "src/SCAoptimizer.h"

#include <iostream>

// There is some randomness in this test (bad), but if 50 attempts fails to shrink this, there is probably an issue anyway.
TEST(SCAOptimizerTest, TestOptimizationReducesSCAwithConstraints) {
	const std::string kFileName = "TempTestFile.txt";
	std::string CON11_3_5 = // an obviously over-sized SCA
		"11 3 5 \n"
		"Con 3 4 \n"
		"3 4 1 0 2 \n"
		"3 2 0 1 4 \n" // next are added lines
		"0 2 3 1 4 \n"
		"3 1 0 4 2 \n"
		"1 0 3 4 2 \n" // done with added lines
		"0 1 2 3 4 \n"
		"2 1 3 4 0 \n"
		"0 3 4 2 1 \n"
		"1 2 0 3 4 \n"
		"2 3 4 0 1 \n"
		"1 3 4 2 0 \n";

	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << CON11_3_5;
	output_file.close();

	SCAoptimizer<long> optimizer(11, 3, 5, kFileName);
	optimizer.ReadInputFromFile(kFileName);

//	EXPECT_TRUE(optimizer.checkSCA());	It's not, because of constraints
	EXPECT_EQ(11, optimizer.SizeOfSCA());
	for (int i = 0; i < 50; ++i) {
		optimizer.DoOneOptimizationPass(false);
	}
//	EXPECT_TRUE(optimizer.checkSCA()); Need to make this a check for SCA with constraints
	EXPECT_LT(optimizer.SizeOfSCA(), 11);
}
