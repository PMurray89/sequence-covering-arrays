#include "gtest/gtest.h"
#include "src/Box4.h"
#include <vector>

TEST(Box4Test, SortingInputsTest) {
	Box4 box1(9, 3, 8, 0);
	std::vector<int> numbers;
	box1.GetNumbersForDebugging(&numbers);
	EXPECT_EQ(4, numbers.size());
	EXPECT_EQ(0, numbers[0]);
	EXPECT_EQ(8, numbers[1]);
	EXPECT_EQ(3, numbers[2]);
	EXPECT_EQ(9, numbers[3]);

	Box4 box2(9, 3, 0, 8);
	box2.GetNumbersForDebugging(&numbers);
	EXPECT_EQ(4, numbers.size());
	EXPECT_EQ(0, numbers[0]);
	EXPECT_EQ(8, numbers[1]);
	EXPECT_EQ(3, numbers[2]);
	EXPECT_EQ(9, numbers[3]);

	Box4 box3(0, 8, 3, 9);
	box3.GetNumbersForDebugging(&numbers);
	EXPECT_EQ(4, numbers.size());
	EXPECT_EQ(0, numbers[0]);
	EXPECT_EQ(8, numbers[1]);
	EXPECT_EQ(3, numbers[2]);
	EXPECT_EQ(9, numbers[3]);

	Box4 box4(8, 0, 9, 3);
	box4.GetNumbersForDebugging(&numbers);
	EXPECT_EQ(4, numbers.size());
	EXPECT_EQ(0, numbers[0]);
	EXPECT_EQ(8, numbers[1]);
	EXPECT_EQ(3, numbers[2]);
	EXPECT_EQ(9, numbers[3]);
}

TEST(Box4Test, GetIndexTest) {
	const int kNumElementsTotal = 15;

	Box4 box1(9, 3, 8, 0);
	// 15^3 * 0 + 15^2 * 8 + 15 * 3 + 9 = 1854
	EXPECT_EQ(1854, box1.GetIndex(kNumElementsTotal));

	Box4 box2(2, 5, 11, 0);
	// 15^3 * 2 + 15^2 * 5 + 15 * 0 + 11 = 7886
	EXPECT_EQ(7886, box2.GetIndex(kNumElementsTotal));
}