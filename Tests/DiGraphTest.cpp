#include "gtest/gtest.h"
#include "src/DiGraph.h"
#include "src/Box4.h"

TEST(DiGraphTest, TrivialDiGraphExtensionTest) {
	// 0 < 1, 2 < 3 < 4
	DiGraph digraph(5);
	digraph.AddEdge(2, 3);
	digraph.AddEdge(3, 4);
	digraph.AddEdge(0, 1);
	digraph.AddEdge(1, 2);

	// Only consistent orderings is:
	// 0 < 1 < 2 < 3 < 4

	std::vector<int> total_order;

	digraph.ExtendToTotalOrder(&total_order);

	// Expect total order to = (0, 1, 2, 3, 4)
	EXPECT_EQ(0, total_order[0]);
	EXPECT_EQ(1, total_order[1]);
	EXPECT_EQ(2, total_order[2]);
	EXPECT_EQ(3, total_order[3]);
	EXPECT_EQ(4, total_order[4]);
}

TEST(DiGraphTest, DiGraphExtensionTest) {
	DiGraph digraph(5);
	// 0,1 < 2 < 3,4
	digraph.AddEdge(0, 2);
	digraph.AddEdge(1, 2);
	digraph.AddEdge(2, 3);
	digraph.AddEdge(2, 4);

	std::vector<int> total_order;

	digraph.ExtendToTotalOrder(&total_order);

	// Only consistent total orders look like
	// (0, 1) or (1, 0)  < 2 <  (3, 4) or (4, 3)
	EXPECT_TRUE((total_order[0] == 0 && total_order[1] == 1) ||
		(total_order[0] == 1 && total_order[1] == 0));
	EXPECT_EQ(2, total_order[2]);
	EXPECT_TRUE((total_order[3] == 3 && total_order[4] == 4) ||
		(total_order[3] == 4 && total_order[4] == 3));
}

TEST(DiGraphTest, NonTrivialDiGraphExtensionTest) {
	DiGraph digraph(5);

	digraph.AddEdge(0, 1);
	digraph.AddEdge(0, 4);
	digraph.AddEdge(3, 1);
	digraph.AddEdge(3, 2);

	std::vector<int> total_order;

	digraph.ExtendToTotalOrder(&total_order);

	std::vector<int> vertex_indices(5);
	for (int index = 0; index < 5; ++index) {
		vertex_indices[total_order[index]] = index;
	}

	EXPECT_LT(vertex_indices[0], vertex_indices[1]);
	EXPECT_LT(vertex_indices[0], vertex_indices[4]);
	EXPECT_LT(vertex_indices[3], vertex_indices[1]);
	EXPECT_LT(vertex_indices[3], vertex_indices[2]);
}

TEST(DiGraphTest, CheckThatExtensionFailsWhenGivenInvalidOrdering) {
	DiGraph digraph(5);

	// Partial order has a cycle (0 < 1 < 2 < 0)
	digraph.AddEdge(0, 1);
	digraph.AddEdge(1, 2);
	digraph.AddEdge(2, 0);
	digraph.AddEdge(3, 4);
	digraph.AddEdge(4, 1);

	std::vector<int> total_order;

	EXPECT_FALSE(digraph.ExtendToTotalOrder(&total_order));
}

TEST(DiGraphTest, MergeDiGraphsTestNoReverse) {
	DiGraph digraph1(5);
	DiGraph digraph2(5);

	digraph1.AddEdge(0, 1);
	digraph1.AddEdge(3, 4);

	digraph2.AddEdge(1, 2);
	digraph2.AddEdge(2, 3);

	digraph1.MergeDiGraphs(digraph2, false);

	EXPECT_TRUE(digraph1.HasEdge(0, 1));
	EXPECT_TRUE(digraph1.HasEdge(3, 4));
	EXPECT_TRUE(digraph1.HasEdge(1, 2));
	EXPECT_TRUE(digraph1.HasEdge(2, 3));
	
	EXPECT_FALSE(digraph1.HasEdge(1, 0));
	EXPECT_FALSE(digraph1.HasEdge(2, 1));
}

TEST(DiGraphTest, MergeDiGraphsTestWithReversal) {
	DiGraph digraph1(5);
	DiGraph digraph2(5);

	digraph1.AddEdge(0, 1);
	digraph1.AddEdge(3, 4);

	digraph2.AddEdge(1, 2);
	digraph2.AddEdge(2, 3);

	digraph1.MergeDiGraphs(digraph2, true);

	EXPECT_TRUE(digraph1.HasEdge(0, 1));
	EXPECT_TRUE(digraph1.HasEdge(3, 4));

	EXPECT_FALSE(digraph1.HasEdge(1, 2));
	EXPECT_FALSE(digraph1.HasEdge(2, 3));

	EXPECT_FALSE(digraph1.HasEdge(1, 0));
	EXPECT_TRUE(digraph1.HasEdge(2, 1));
	EXPECT_TRUE(digraph1.HasEdge(3, 2));
}

TEST(DiGraphTest, SearchForDirectedPathTest) {
	DiGraph digraph(6);

	digraph.AddEdge(0, 1);
	digraph.AddEdge(1, 5);

	EXPECT_EQ(1, digraph.PathBetween(0, 4, 3, 5));
	EXPECT_EQ(0, digraph.PathBetween(0, 1, 3, 4));
	EXPECT_EQ(-1, digraph.PathBetween(4, 5, 2, 1));

	digraph.Clear();
	digraph.AddEdge(1, 2);
	digraph.AddEdge(2, 3);
	digraph.AddEdge(0, 2);
	EXPECT_EQ(1, digraph.PathBetween(0, 4, 1, 3));
	EXPECT_EQ(0, digraph.PathBetween(0, 4, 1, 5));
}

TEST(DiGraphTest, MergeWithBox4Test) {
	DiGraph digraph(6);
	Box4 box(0, 1, 4, 5);
	digraph.AddBoxRelation(box, false);
	EXPECT_TRUE(digraph.HasEdge(0, 4));
	EXPECT_TRUE(digraph.HasEdge(0, 5));
	EXPECT_TRUE(digraph.HasEdge(1, 4));
	EXPECT_TRUE(digraph.HasEdge(1, 5));

	EXPECT_FALSE(digraph.HasEdge(4, 0));
	EXPECT_FALSE(digraph.HasEdge(5, 0));
	EXPECT_FALSE(digraph.HasEdge(4, 1));
	EXPECT_FALSE(digraph.HasEdge(5, 1));

	digraph.Clear();
	digraph.AddBoxRelation(box, true); // reverse box
	EXPECT_FALSE(digraph.HasEdge(0, 4));
	EXPECT_FALSE(digraph.HasEdge(0, 5));
	EXPECT_FALSE(digraph.HasEdge(1, 4));
	EXPECT_FALSE(digraph.HasEdge(1, 5));

	EXPECT_TRUE(digraph.HasEdge(4, 0));
	EXPECT_TRUE(digraph.HasEdge(5, 0));
	EXPECT_TRUE(digraph.HasEdge(4, 1));
	EXPECT_TRUE(digraph.HasEdge(5, 1));
}