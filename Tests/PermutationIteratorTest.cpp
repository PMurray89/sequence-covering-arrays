#include "gtest/gtest.h"
#include "src/PermutationIterator.h"
#include <vector>
#include <sstream>

TEST(PermutationIteratorTest, TestPermutationIterating) {
	PermutationIterator<int> permutation_iterator(5, 3);
	int integer_array[5] = { 0, 1, 2, 3, 4 };

	permutation_iterator.setArray(integer_array);
	permutation_iterator.resetPerm();
	EXPECT_EQ(0, permutation_iterator.at(0));
	EXPECT_EQ(1, permutation_iterator.at(1));
	EXPECT_EQ(2, permutation_iterator.at(2));
	permutation_iterator.nextPerm();
	EXPECT_EQ(0, permutation_iterator.at(0));
	EXPECT_EQ(1, permutation_iterator.at(1));
	EXPECT_EQ(3, permutation_iterator.at(2));

	permutation_iterator.nextPerm();
	EXPECT_EQ(0, permutation_iterator.at(0));
	EXPECT_EQ(1, permutation_iterator.at(1));
	EXPECT_EQ(4, permutation_iterator.at(2));

	permutation_iterator.nextPerm();
	EXPECT_EQ(0, permutation_iterator.at(0));
	EXPECT_EQ(2, permutation_iterator.at(1));
	EXPECT_EQ(3, permutation_iterator.at(2));

	for (int i = 0; i < 6; ++i) {
		permutation_iterator.nextPerm();
	}
	EXPECT_EQ(2, permutation_iterator.at(0));
	EXPECT_EQ(3, permutation_iterator.at(1));
	EXPECT_EQ(4, permutation_iterator.at(2));
}

TEST(PermutationIteratorTest, TestNumberOfPermutations) {
	PermutationIterator<int> permutation_iterator(5, 3);
	int integer_array[5] = { 0, 1, 2, 3, 4 };

	permutation_iterator.setArray(integer_array);
	permutation_iterator.resetPerm();
	int num_permutations = 0;
	do {
		++num_permutations;
	} while (permutation_iterator.nextPerm());

	// Expect (5 choose 3) = 10 permutations
	EXPECT_EQ(10, num_permutations);
}
/* Permutation Indices
0 = (0, 1, 2)
1 = (0, 1, 3)
2 = (0, 1, 4)
3 = (0, 2, 1)
4 = (0, 2, 3)
5 = (0, 2, 4)
6 = (0, 3, 1)
7 = (0, 3, 2)
8 = (0, 3, 4)
9 = (0, 4, 1)
10 = (0, 4, 2)
11 = (0, 4, 3)
12 = (1, 0, 2)
13 = (1, 0, 3)
14 = (1, 0, 4)
15 = (1, 2, 0)
16 = (1, 2, 3)
17 = (1, 2, 4)
18 = (1, 3, 0)
19 = (1, 3, 2)
20 = (1, 3, 4)
21 = (1, 4, 0)
22 = (1, 4, 2)
23 = (1, 4, 3)
... etc up until (5 choose 3)*3! = 60
*/
TEST(PermutationIteratorTest, TestPermutationIndexing) {
	PermutationIterator<int> permutation_iterator(5, 3);
	int integer_array[5] = { 0, 1, 2, 3, 4 };

	permutation_iterator.setArray(integer_array);
	permutation_iterator.resetPerm();

	// (0, 1, 2) is lowest perm, so = 0
	EXPECT_EQ(0, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (0, 1, 3) = 1
	EXPECT_EQ(1, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (0, 1, 4) = 2
	EXPECT_EQ(2, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (0, 2, 3) = 4
	EXPECT_EQ(4, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (0, 2, 4) = 5
	EXPECT_EQ(5, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (0, 3, 4) = 8
	EXPECT_EQ(8, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (1, 2, 3) = 16
	EXPECT_EQ(16, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (1, 2, 4) = 17
	EXPECT_EQ(17, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (1, 3, 4) = 20
	EXPECT_EQ(20, permutation_iterator.getPermutationIndex());
	permutation_iterator.nextPerm();

	// (2, 3, 4) = 32
	EXPECT_EQ(32, permutation_iterator.getPermutationIndex());

	// Permutations are done
	EXPECT_FALSE(permutation_iterator.nextPerm());
}

TEST(PermutationIteratorTest, TestPermutationLookupByIndex) {
	PermutationIterator<int> permutation_iterator(5, 3);

	std::vector<int> permutation;
	std::stringstream ss;

	permutation_iterator.GetPermutationFromIndex(0, &permutation);
	for (int i = 0; i < 3; ++i) {
		ss << permutation[i] << " ";
	}
	EXPECT_EQ("0 1 2 ", ss.str());
	ss.str("");
	ss.clear();

	permutation_iterator.GetPermutationFromIndex(1, &permutation);
	for (int i = 0; i < 3; ++i) {
		ss << permutation[i] << " ";
	}
	EXPECT_EQ("0 1 3 ", ss.str());
	ss.str("");
	ss.clear();

	permutation_iterator.GetPermutationFromIndex(2, &permutation);
	for (int i = 0; i < 3; ++i) {
		ss << permutation[i] << " ";
	}
	EXPECT_EQ("0 1 4 ", ss.str());
	ss.str("");
	ss.clear();

	permutation_iterator.GetPermutationFromIndex(32, &permutation);
	for (int i = 0; i < 3; ++i) {
		ss << permutation[i] << " ";
	}
	EXPECT_EQ("2 3 4 ", ss.str());
	ss.str("");
	ss.clear();

	permutation_iterator.GetPermutationFromIndex(17, &permutation);
	for (int i = 0; i < 3; ++i) {
		ss << permutation[i] << " ";
	}
	EXPECT_EQ("1 2 4 ", ss.str());
	ss.str("");
	ss.clear();
}