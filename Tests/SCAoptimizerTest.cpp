#include "gtest/gtest.h"
#include "src/SCAoptimizer.h"

#include <iostream>

// There is some randomness in this test (bad), but if 50 attempts fails to shrink this, there is probably an issue anyway.
TEST(SCAOptimizerTest, TestOptimizationReducesSCAsize) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA10_3_4 = // an obviously over-sized SCA
		"10 3 4\n"
		"3 1 0 2 \n"
		"0 1 2 3 \n"
		"0 2 1 3 \n"
		"3 2 1 0 \n"
		"0 2 1 3 \n"
		"1 2 0 3 \n"
		"1 3 0 2 \n"
		"2 3 0 1 \n"
		"3 2 1 0 \n"
		"0 3 1 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA10_3_4;
	output_file.close();

	SCAoptimizer<long> optimizer(10, 3, 4, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.checkSCA());
	EXPECT_EQ(10, optimizer.SizeOfSCA());
	for (int i = 0; i < 50; ++i) {
		optimizer.DoOneOptimizationPass(false);
	}
	EXPECT_TRUE(optimizer.checkSCA());
	EXPECT_LT(optimizer.SizeOfSCA(), 10);
}
TEST(SCAOptimizerTest, TestOptimizationPassPreservesSCAProperty) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA9_3_4 =
		"9 3 4\n"
		"0 1 2 3 \n"
		"0 2 1 3 \n"
		"3 2 1 0 \n"
		"0 2 1 3 \n"
		"1 2 0 3 \n"
		"1 3 0 2 \n"
		"2 3 0 1 \n"
		"3 2 1 0 \n"
		"0 3 1 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA9_3_4;
	output_file.close();

	SCAoptimizer<long> optimizer(9, 3, 4, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.checkSCA());
	for (int i = 0; i < 5; ++i) {
//		optimizer.printSCA();
//		std::cout << std::endl;
		optimizer.DoOneOptimizationPass(false);
		EXPECT_TRUE(optimizer.checkSCA());
	}
}

TEST(SCAOptimizerTest, TestLinearExtensionsOperationPreservesSCAProperty) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA9_3_4 =
		"9 3 4\n"
		"0 1 2 3 \n"
		"0 2 1 3 \n"
		"3 2 1 0 \n"
		"0 2 1 3 \n"
		"1 2 0 3 \n"
		"1 3 0 2 \n"
		"2 3 0 1 \n"
		"3 2 1 0 \n"
		"0 3 1 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA9_3_4;
	output_file.close();

	SCAoptimizer<long> optimizer(9, 3, 4, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.checkSCA());
	for (int i = 0; i < 5; ++i) {
//		optimizer.printSCA();
//		std::cout << std::endl;
		optimizer.ChangeAllPermutationsViaLinearExtensions();
		EXPECT_TRUE(optimizer.checkSCA());
	}
}

TEST(SCAOptimizerTest, TestOptimizationCanRemoveRedundantPermutations) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA7_3_4 =
		"7 3 4\n"
		"0 3 1 2 \n"
		"0 2 1 3 \n"
		"1 2 0 3 \n"
		"1 3 0 2 \n"
		"2 3 0 1 \n"
		"3 2 1 0 \n"
		"0 3 1 2 \n"; // this one is redundant
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA7_3_4;
	output_file.close();

	SCAoptimizer<long> optimizer(7, 3, 4, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.checkSCA());
	EXPECT_EQ(7, optimizer.SizeOfSCA());

	optimizer.DoOneOptimizationPass(false);
	EXPECT_TRUE(optimizer.checkSCA());
	EXPECT_EQ(6, optimizer.SizeOfSCA());
}

TEST(SCAOptimizerTest, TestReadingAndWritingArrays) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA6_3_4 =
		"6 3 4\n"
		"0 2 1 3 \n"
		"1 2 0 3 \n"
		"1 3 0 2 \n"
		"2 3 0 1 \n"
		"3 2 1 0 \n"
		"0 3 1 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA6_3_4;
	output_file.close();


	SCAoptimizer<long> optimizer(6, 3, 4, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	std::string output_string;
	optimizer.WriteSCAToString(false, &output_string);

	EXPECT_EQ(SCA6_3_4, output_string);
}

TEST(SCAOptimizerTest, CheckThatActualSCAisDetectedToBeValidTest) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA6_3_4 =
		"6 3 4\n"
		"0 2 1 3 \n"
		"1 2 0 3 \n"
		"1 3 0 2 \n"
		"2 3 0 1 \n"
		"3 2 1 0 \n"
		"0 3 1 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA6_3_4;
	output_file.close();

	SCAoptimizer<long> optimizer(6, 3, 4, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.checkSCA());
}

TEST(SCAOptimizerTest, CheckThatBadSCAisDetectedToBeInvalidTest) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA6_3_4 =
		"6 3 4\n"
		"0 2 1 3 \n"
		"2 1 0 3 \n"
		"1 3 0 2 \n"
		"2 3 0 1 \n"
		"3 2 1 0 \n"
		"3 0 1 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA6_3_4;
	output_file.close();

	SCAoptimizer<long> optimizer(6, 3, 4, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_FALSE(optimizer.checkSCA());
}

TEST(SCAOptimizerTest, CheckThatValidSCAwithAvoidsIsVerified) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA_avoids_8_3_7 =
		"8 3 7\n"
		"Avd 3 0 \n"
		"Avd 4 1 \n"
		"Avd 5 2 \n"
		"1 4 2 0 6 5 3 \n"
		"6 0 2 3 1 5 4 \n"
		"2 5 0 1 3 6 4 \n"
		"0 4 3 5 6 1 2 \n"
		"3 2 4 6 5 1 0 \n"
		"1 5 6 4 0 3 2 \n"
		"6 1 2 0 4 3 5 \n"
		"5 3 1 4 0 2 6 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA_avoids_8_3_7;
	output_file.close();

	SCAoptimizer<long> optimizer(8, 3, 7, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.checkSCA());
}

TEST(SCAOptimizerTest, CheckThatBADSCAwithAvoidsIsCaught) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA_avoids_8_3_7 =
		"8 3 7\n"
		"Avd 3 0 \n"
		"Avd 4 1 \n"
		"Avd 5 2 \n"
		"1 4 2 0 6 5 3 \n"
		"6 0 2 3 1 5 4 \n"
		"2 5 0 1 3 6 4 \n"
		"0 4 3 5 6 1 2 \n"
		"3 2 4 6 5 1 0 \n"
		"1 5 6 4 0 3 2 \n"
		"6 1 2 0 4 3 5 \n"
		"5 3 1 4 6 2 0 \n"; // misses 0 2 6
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA_avoids_8_3_7;
	output_file.close();

	SCAoptimizer<long> optimizer(8, 3, 7, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_FALSE(optimizer.checkSCA());
}

TEST(SCAOptimizerTest, CheckThatValidSCAwithConstraintsIsVerified) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA_constraints_13_3_9 =
		"13 3 9\n"
		"Con 0 4 \n"
		"Con 1 5 \n"
		"Con 2 6 \n"
		"Con 3 7 \n"
		"1 5 3 8 7 0 2 6 4 \n"
		"2 0 3 4 6 7 8 1 5 \n"
		"0 1 4 2 8 5 6 3 7 \n"
		"3 7 2 1 6 5 0 8 4 \n"
		"8 0 4 3 1 5 2 7 6 \n"
		"2 6 8 1 3 0 7 5 4 \n"
		"1 5 0 3 7 4 8 2 6 \n"
		"2 6 0 4 1 5 3 7 8 \n"
		"3 7 8 1 5 2 6 0 4 \n"
		"3 8 7 0 4 2 6 1 5 \n"
		"1 0 5 4 2 6 8 3 7 \n"
		"8 2 6 3 1 7 0 4 5 \n"
		"0 4 3 1 5 8 7 2 6 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA_constraints_13_3_9;
	output_file.close();

	SCAoptimizer<long> optimizer(13, 3, 9, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.checkSCA());
}

TEST(SCAOptimizerTest, CheckThatBADSCAwithConstraintsIsCaught) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA_constraints_12_3_9 =
		"12 3 9\n"
		"Con 0 4 \n"
		"Con 1 5 \n"
		"Con 2 6 \n"
		"Con 3 7 \n"
		"1 5 3 8 7 0 2 6 4 \n"
		"2 0 3 4 6 7 8 1 5 \n"
		"0 1 4 2 8 5 6 3 7 \n"
		"3 7 2 1 6 5 0 8 4 \n"
		"8 0 4 3 1 5 2 7 6 \n"
		"2 6 8 1 3 0 7 5 4 \n"
		"1 5 0 3 7 4 8 2 6 \n"
		"2 6 0 4 1 5 3 7 8 \n"
		"3 7 8 1 5 2 6 0 4 \n"
		"3 8 7 0 4 2 6 1 5 \n"
		"1 0 5 4 2 6 8 3 7 \n"
		"8 2 6 3 1 7 0 4 5 \n";
//		"0 4 3 1 5 8 7 2 6 \n"; deleting last row
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA_constraints_12_3_9;
	output_file.close();

	SCAoptimizer<long> optimizer(12, 3, 9, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_FALSE(optimizer.checkSCA());
}

TEST(SCAOptimizerTest, CheckThatBADSCAwithConstraintsThatViolatedTheseIsCaught) {
	const std::string kFileName = "TempTestFile.txt";
	std::string SCA_constraints_14_3_9 =
		"14 3 9\n"
		"Con 0 4 \n"
		"Con 1 5 \n"
		"Con 2 6 \n"
		"Con 3 7 \n"
		"1 5 3 8 7 0 2 6 4 \n"
		"2 0 3 4 6 7 8 1 5 \n"
		"0 1 4 2 8 5 6 3 7 \n"
		"3 7 2 1 6 5 0 8 4 \n"
		"8 0 4 3 1 5 2 7 6 \n"
		"2 6 8 1 3 0 7 5 4 \n"
		"1 5 0 3 7 4 8 2 6 \n"
		"2 6 0 4 1 5 3 7 8 \n"
		"3 7 8 1 5 2 6 0 4 \n"
		"3 8 7 0 4 2 6 1 5 \n"
		"1 0 5 4 2 6 8 3 7 \n"
		"8 2 6 3 1 7 0 4 5 \n"
		"0 4 3 1 5 8 7 2 6 \n" // adding a new row
		"4 0 3 1 5 8 7 2 6 \n"; // This row violates 0 < 4
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << SCA_constraints_14_3_9;
	output_file.close();

	SCAoptimizer<long> optimizer(14, 3, 9, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_FALSE(optimizer.checkSCA());
}


TEST(BoxOptimizationTest, ActualBoxDetectedToBeValidTest) {
	const std::string kFileName = "TempTestFile.txt";
	std::string BOX3_5 =
		"3 4 5 \n"
		"2 1 0 3 4 \n"
		"3 1 0 2 4 \n"
		"4 1 0 3 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << BOX3_5;
	output_file.close();

	SCAoptimizer<long> optimizer(3, 4, 5, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.CheckBox());
}

TEST(BoxOptimizationTest, BadBoxisDetectedToBeInvalidTest) {
	const std::string kFileName = "TempTestFile.txt";
	std::string BOX3_5 =
		"3 4 5 \n"
		"2 0 1 3 4 \n"
		"3 1 0 2 4 \n"
		"4 1 0 3 2 \n";
	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << BOX3_5;
	output_file.close();

	SCAoptimizer<long> optimizer(3, 4, 5, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	// Lacks (2,1) sep (0,3)
	EXPECT_FALSE(optimizer.CheckBox());
}

TEST(BoxOptimizationTest, TestLinearExtensionsOperationPreservesBOXProperty) {
	const std::string kFileName = "TempTestFile.txt";
	std::string BOX8_13 =
		"10 4 13 \n"
		"8 10 12 5 2 7 11 0 6 9 3 4 1 \n"
		"0 1 2 3 4 5 6 7 8 9 10 11 12 \n"
		"2 1 0 5 4 3 8 7 6 11 10 9 12 \n"
		"4 8 9 5 0 2 11 3 7 6 12 1 10 \n"
		"8 1 12 9 7 0 2 3 6 5 11 4 10 \n"
		"8 6 5 1 7 9 10 2 3 0 11 4 12 \n"
		"6 0 10 3 8 12 7 4 5 2 1 9 11 \n"
		"8 11 6 2 12 4 7 3 1 0 5 10 9 \n"
		"6 9 4 5 12 7 11 3 8 2 0 10 1 \n"
		"5 0 7 1 11 10 6 4 2 8 3 12 9 \n";

	std::ofstream output_file;
	output_file.open(kFileName.c_str());
	output_file << BOX8_13;
	output_file.close();

	SCAoptimizer<long> optimizer(10, 4, 13, kFileName);
	optimizer.ReadInputFromFile(kFileName);

	EXPECT_TRUE(optimizer.CheckBox());
	for (int i = 0; i < 5; ++i) {
		//		optimizer.printSCA();
		//		std::cout << std::endl;
		optimizer.UpdatePermutationsAndStillCoverBox4s();
		EXPECT_TRUE(optimizer.CheckBox());
	}
}