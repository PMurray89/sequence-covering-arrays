#ifndef ARRAY2D_H
#define ARRAY2D_H

#include <assert.h>
#include <iostream>

template <typename T>
class Array2D {
	private:
		unsigned width;
		unsigned height;
		T* linearArray;
	public:
		Array2D() {
			height = 0;
			width = 0;
			linearArray = NULL;
		}
		void initialize(unsigned ht, unsigned wdt) {
			height = ht;
			width = wdt;
			delete [] linearArray;
			linearArray = new T[wdt * ht];
		}
		Array2D(unsigned ht, unsigned wdt) {
			height = ht;
			width = wdt;
			linearArray = new T[wdt * ht]();
		}
		~Array2D() {
//			std::cout << "Deleting Array2D\n";
			delete [] linearArray;
		}

		const T& operator() (unsigned x1, unsigned x2) const
		{
			assert(x1 < height);
			assert(x2 < width);
			return linearArray[ x1 * width + x2];
		}
		T& operator() (unsigned x1, unsigned x2) {
			assert(x1 < height);
			assert(x2 < width);
			return linearArray[ x1 * width + x2];
		}
		// Provides read-only access to a sub-array
		const T* operator[] (unsigned x1) const {
			return linearArray  + x1 * width ;
		}
};

#endif // ARRAY2D_H