#ifndef COVERINGTYPE_H_
#define COVERINGTYPE_H_

#include "PermutationIterator.h"

#include <utility>
#include <vector>

class CoveringType {
public:
	std::pair<int, int> pair1;
	std::pair<int, int> pair2;
	std::pair<int, int> original_pair1;
	std::pair<int, int> original_pair2;
public:
	CoveringType(int pair1_first, int pair1_second, int pair2_first, int pair2_second) :
		original_pair1(pair1_first, pair1_second),
		original_pair2(pair2_first, pair2_second),
		pair1(pair1_first, pair1_second), pair2(pair2_first, pair2_second) {
		if (pair1.first > pair1.second) {
			std::swap(pair1.first, pair1.second);
		}
		if (pair2.first > pair2.second) {
			std::swap(pair2.first, pair2.second);
		}
		if (pair1.second > pair2.second) {
			std::swap(pair1, pair2);
		}
	}
	template<typename T>
	CoveringType(const PermutationIterator<T>& perm_iterator) :
// CROSS		CoveringType(perm_iterator.at(0), perm_iterator.at(2), perm_iterator.at(1), perm_iterator.at(3)) {}
		CoveringType(perm_iterator.at(0), perm_iterator.at(3),
		perm_iterator.at(1), perm_iterator.at(2)) {}

	void GetNumbersForDebugging(std::vector<int> * box_numbers) {
		box_numbers->resize(4);
		box_numbers->at(0) = pair1.first;
		box_numbers->at(1) = pair1.second;
		box_numbers->at(2) = pair2.first;
		box_numbers->at(3) = pair2.second;
	}
	int GetIndex(int num_elements_total) {
		return pair2.second + num_elements_total * pair2.first
			+ num_elements_total * num_elements_total * pair1.second
			+ num_elements_total * num_elements_total * num_elements_total * pair1.first;
	}
};

#endif // COVERINGTYPE_H_