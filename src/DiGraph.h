#ifndef DIGRAPH_H
#define DIGRAPH_H

#include <assert.h>
#include <iostream>
#include <vector>

#include "Array2D.h"
#include "Box4.h"

class DiGraph {
private:
	Array2D<unsigned int> children_adj_matrix_;
	std::vector<std::vector<int> > children_adj_lists_;
	std::vector<int> number_of_parents_;
	const int num_vertices_;
	unsigned int current_run_;
public:
	DiGraph(int number_of_vertices) :
		children_adj_matrix_(number_of_vertices, number_of_vertices),
		children_adj_lists_(number_of_vertices, std::vector<int>()),
		number_of_parents_(number_of_vertices, 0),
		num_vertices_(number_of_vertices), current_run_(1) {}

	// Adds the constraint lesser_vertex < larger_vertex to the partial ordering
	// returns false if this relation was previously included, true if it is 
	// successfully added
	inline bool AddLessThanOrdering(int lesser_vertex, int larger_vertex) {
		return AddEdge(lesser_vertex, larger_vertex);
	}
	inline bool AddEdge(int v, int u) {
		if (children_adj_matrix_(v, u) == current_run_) {
			return false;
		}
		children_adj_matrix_(v, u) = current_run_;
		children_adj_lists_[v].push_back(u);
		++number_of_parents_[u];
		return true;
	}
	inline bool HasEdge(int v, int u) const {
		return children_adj_matrix_(v, u) == current_run_;
	}
	void Clear() {
		++current_run_;
		for (auto& list : children_adj_lists_) {
			list.clear();
		}
	}

	// Extends partial order to a total order and resets the internal state.
	// Returns true if successful, false if unsuccessful.
	bool ExtendToTotalOrder(std::vector<int>* total_order_output) const {
		total_order_output->clear();
		std::vector<int> list_of_free_vertices;

		for (int vertex = 0; vertex < num_vertices_; ++vertex) {
			if (number_of_parents_[vertex] == 0) {
				// A vertex with no parents can be at the start of our ordering
				list_of_free_vertices.push_back(vertex);
			}
		}

		std::vector<int> num_parents(number_of_parents_);
		while (!list_of_free_vertices.empty()) {
			int rand_index = std::rand() % list_of_free_vertices.size();
			int chosen_vertex = list_of_free_vertices[rand_index];

			list_of_free_vertices[rand_index] = list_of_free_vertices.back();
			list_of_free_vertices.pop_back();

			// For all children of the chosen vertex
			for (int child_vertex : children_adj_lists_[chosen_vertex]) {
				if (--num_parents[child_vertex] == 0) {
					// if chosen_vertex was this child's only direct relation, it is now a free vertex
					list_of_free_vertices.push_back(child_vertex);
				}
			}
			total_order_output->push_back(chosen_vertex);
		}
		return total_order_output->size() == num_vertices_;
	}
	void MergeDiGraphs(const DiGraph& graph_to_add, const bool reverse_order) {
		assert(graph_to_add.num_vertices_ == num_vertices_);
		for (int parent_vertex = 0; parent_vertex < num_vertices_; ++parent_vertex) {
			for (auto child_vertex : graph_to_add.children_adj_lists_[parent_vertex]) {
				if (reverse_order) {
					AddEdge(child_vertex, parent_vertex);
				}
				else {
					AddEdge(parent_vertex, child_vertex);
				}
			}
		}
	}
	void AddBoxRelation(const Box4& box_to_add, const bool reverse_order) {
		if (reverse_order) {
			AddEdge(box_to_add.original_pair2.first, box_to_add.original_pair1.first);
			AddEdge(box_to_add.original_pair2.first, box_to_add.original_pair1.second);
			AddEdge(box_to_add.original_pair2.second, box_to_add.original_pair1.first);
			AddEdge(box_to_add.original_pair2.second, box_to_add.original_pair1.second);
		}
		else {
			AddEdge(box_to_add.original_pair1.first, box_to_add.original_pair2.first);
			AddEdge(box_to_add.original_pair1.first, box_to_add.original_pair2.second);
			AddEdge(box_to_add.original_pair1.second, box_to_add.original_pair2.first);
			AddEdge(box_to_add.original_pair1.second, box_to_add.original_pair2.second);
		}
	}
	// Returns true iff there is a directed path between {v1, v2} and {u1, u2}
	bool HasDirectedPath(const int v1, const int v2, const int u1, const int u2) const {
		// new, free and memmove were taking up 60%+ of the time prior to making 
		// these objects static
		static std::vector<bool> have_seen(num_vertices_);
		static std::vector<int> queue_of_vertices_to_visit;
		have_seen.clear(); have_seen.resize(num_vertices_);
		queue_of_vertices_to_visit.clear();

		queue_of_vertices_to_visit.push_back(v1);
		if (v1 != v2) {
			queue_of_vertices_to_visit.push_back(v2);
		}
		have_seen[v1] = true;
		have_seen[v2] = true;
		for (unsigned int index = 0; index < queue_of_vertices_to_visit.size(); ++index) {
			const int parent_vertex = queue_of_vertices_to_visit[index];
			for (const int child_vertex : children_adj_lists_[parent_vertex]) {
				if (!have_seen[child_vertex]) {
					if (child_vertex == u1 || child_vertex == u2) {
						return true;
					}
					have_seen[child_vertex] = true;
					queue_of_vertices_to_visit.push_back(child_vertex);
				}
			}
		}
		return false;
	}
	int PathBetween(const int v1, const int v2, const int u1, const int u2) {
		if (HasDirectedPath(v1, v2, u1, u2)) {
			return 1;
		}
		else if (HasDirectedPath(u1, u2, v1, v2)) {
			return -1;
		}
		return 0;
	}
	// Returns 1 if they're correlated, -1 if reverse correlated, 0 otherwise.
	int CorrelatedWithBox(const Box4& box) {
		return PathBetween(box.original_pair1.first, box.original_pair1.second,
			box.original_pair2.first, box.original_pair2.second);
	}
	void PrintGraph() {
		std::cout << num_vertices_ << " vertices." << std::endl;
		for (int i = 0; i < num_vertices_; ++i) {
			std::cout << i << ": " << children_adj_lists_[i].size() << ": ";
			for (const int v : children_adj_lists_[i]) {
				std::cout << v << ", ";
			}
			std::cout << std::endl;
		}
		std::cout << "Done with graph" << std::endl;
	}
};
/*
void MergeListOfPartialOrders(std::vector<DiGraph*>& partial_order_list, DiGraph* merged_partial_order) {
	if (partial_order_list.size() == 2) {
		merged_partial_order->MergeDiGraphs(*partial_order_list[0], false);
		merged_partial_order->MergeDiGraphs(*partial_order_list[1], true);
		delete partial_order_list[0];
		delete partial_order_list[1];
		partial_order_list.clear();
	}
	else {
		for (const DiGraph* partial_order : partial_order_list) {
			merged_partial_order->MergeDiGraphs(*partial_order, false);
			delete partial_order;
		}
		partial_order_list.clear();
	}
}
*/

#endif  // DIGRAPH_H