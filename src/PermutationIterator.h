#ifndef PERMUTATION_ITERATOR_H_
#define PERMUTATION_ITERATOR_H_

#include<iostream>
#include<string>
#include <sstream>
#include<fstream>
#include <vector>
using std::cout;
using std::endl;

namespace {
	template<typename T>
	T NpermK(int N, int k) {
		T result = N;
		for (int i = N - 1; i > N - k; --i) {
			result *= i;
		}
		return result;
	}
	template<typename T>
	T NtoK(int N, int k) {
		T result = N;
		for (int i = 1; i < k; ++i) {
			result *= N;
		}
		return result;
	}
}


template<typename T>
class PermutationIterator {
	private:
		int N, K;
		int * permIndices;
		const int * currentArray;
		T * permutations;
	public:
		PermutationIterator(int n, int k) {
			N = n;
			K = k;
			permutations = new T[k];
			permutations[k - 1] = 1; //N - k + 1;
			for (int i = k - 2; i >= 0; --i) {
				permutations[i] = (N - i - 1)*permutations[i + 1];
			}
			permIndices = new int[k];
		}
		~PermutationIterator()
		{
//			cout << "Deleting PermutationIterator\n";
			delete [] permIndices;
			delete [] permutations;
		}
		void resetPerm() {
			for (int i = 0; i < K; ++i) {
				permIndices[i] = i;
			}
		}
		bool nextPerm() {
			if (permIndices[K - 1] < N - 1) {
				++permIndices[K - 1];
				return true;
			}
			for (int j = K - 2; j >= 0; --j) {
				if (permIndices[j] < permIndices[j + 1] - 1) {
					++permIndices[j];
					for (int k = j + 1; k < K; ++k) {
						permIndices[k] = permIndices[j] + k - j;
					}
					return true;
				}
			}
			return false;
		}
		void setArray(const int * arr) {
			currentArray = arr;
		}
		
		static T getSizeOfLargestPermutationIndex(int n, int k) {
			return NtoK<T>(n, k);
		}
		
		T getIndexOfPermutation() const {
			return getPermutationAsBaseN();
		}
		// Returns the permutation as a base N number. This is less compact memory wise,
		// but may result in faster indexing.
		T getPermutationAsBaseN() const {
			T index = currentArray[permIndices[K - 1]];
			T exponent = N;
			index = index + exponent * currentArray[permIndices[K - 2]];
			for (int i = K - 3; i >= 0; --i) {
				exponent = exponent * N;
				index = index + exponent * currentArray[permIndices[i]];
			}

			return index;
		}
		T getPermutation() const {
			T index = 0;
			int positionOffset;
			for (int i = 0; i < K; ++i) {
				positionOffset = 0;
				for (int j = 0; j < i; ++j) {
					positionOffset += (currentArray[permIndices[i]] > currentArray[permIndices[j]]);
				}
//				std::cout << currentArray[permIndices[i]] << " (" << positionOffset << ")  ";
				index += permutations[i] * (currentArray[permIndices[i]] - positionOffset);
			}
			return index;
		}
		T getPermutationIndex() const {
			T index = permutations[0] * currentArray[permIndices[0]];
			int positionOffset, currentNumber;
			for (int i = 1; i < K; ++i) {
				positionOffset = 0;
				currentNumber = currentArray[permIndices[i]];
				for (int j = 0; j < i; ++j) {
					positionOffset += (currentNumber > currentArray[permIndices[j]]);
				}
				index += (currentNumber - positionOffset) * permutations[i];
			}
			return index;
		}
		void GetPermutationFromIndex(T index, std::vector<int>* permutation_vector) {
			std::vector<bool> values_seen(N);
			permutation_vector->clear();
			permutation_vector->push_back(index / permutations[0]);
			index -= permutation_vector->back() * permutations[0];
			values_seen[permutation_vector->back()] = true;
			int positionOffset;
			for (int i = 1; i < K; ++i) {
				int next_offset = index / permutations[i];
				index -= next_offset * permutations[i];
				int next_value = 0;
				int value_index = -1;
				while (next_offset >= 0 && value_index < N) {
					++value_index;
					if (values_seen[value_index] == false) {
						--next_offset;
					}
				}
				// There's a problem with the given index if this fails
				assert(value_index < N);
				permutation_vector->push_back(value_index);
				values_seen[value_index] = true;
			}
		}
		T getPermutationIndexNoComp() const {
			T index = permutations[0] * currentArray[permIndices[0]];
			int positionOffset, currentNumber;
			for (int i = 1; i < K; ++i) {
				positionOffset = 0;
				currentNumber = currentArray[permIndices[i]];
				for (int j = 0; j < i; ++j) {
					positionOffset += (currentArray[permIndices[j]] - currentNumber) >> 31;
				}
				index += (currentNumber + positionOffset) * permutations[i];
			}
			return index;
		}
		T getPermutationIndex2() const {
			int currentNumber = currentArray[permIndices[0]];
			int nextNum = currentArray[permIndices[1]];
			T index;
			index = (nextNum > currentNumber) ? (permutations[0] * currentNumber 
				+ permutations[1] * (nextNum - 1)) : (permutations[0] * currentNumber
				+ permutations[1] * nextNum);
			int positionOffset;
			for (int i = 2; i < K; ++i) {
				currentNumber = currentArray[permIndices[i]];
				positionOffset = 0;
				for (int j = 0; j < i; ++j) {
					positionOffset += (currentNumber > currentArray[permIndices[j]]);
				}
				index += (currentNumber - positionOffset) * permutations[i];
			}
			return index;
		}
		// I believe the idea below is fatally flawed
		T getPermutationIndex2WithChecking() const {
			int previousNum = currentArray[permIndices[0]];
			int currentNumber = currentArray[permIndices[1]];
			int positionOffset = 0;
			T index = (currentNumber > previousNum) ? (permutations[0] * previousNum 
				+ permutations[1] * (currentNumber - ++positionOffset)) : 
				(permutations[0] * previousNum + permutations[1] * currentNumber);
			for (int i = 2; i < K; ++i) {
				previousNum = currentNumber;
				currentNumber = currentArray[permIndices[i]];
				if (currentNumber > previousNum) {
					index += (currentNumber - ++positionOffset) * permutations[i];
					continue;
				}
//				currentNumber = currentArray[permIndices[i]];
				positionOffset = 0;
				for (int j = 0; j < i; ++j) {
					positionOffset += (currentNumber > currentArray[permIndices[j]]);
				}
				index += (currentNumber - positionOffset) * permutations[i];
			}
			return index;
		}
		T getPermutationIndexSwitchCase() const {
//			switch (K) {
//			case 3:
//				int positionOffset = 0;
				int num1 = currentArray[permIndices[0]];
				int num2 = currentArray[permIndices[1]];
				int num3 = currentArray[permIndices[2]];
				T index = permutations[0] * num1;
				if (num2 > num1) {
					index += permutations[1] * (num2 - 1);
					if (num3 > num2) {
						return index + permutations[2] * (num3 - 2);
					}
					else if (num3 > num1) {
						return index + permutations[2] * (num3 - 1);
					}
					else {
						return index + permutations[2] * num3;
					}
				}
				else {
					index += permutations[1] * num2;
					if (num3 < num2) {
						return index + permutations[2] * num3;
					}
					else if (num3 < num1) {
						return index + permutations[2] * (num3 - 1);
					}
					else {
						return index + permutations[2] * (num3 - 2);
					}
				}
//				positionOffset += (num1 >)
//				break;
/*			case 4:
				int positionOffset = 0;
				int num1 = currentArray[permIndices[0]];
				int num2 = currentArray[permIndices[1]];
				int num3 = currentArray[permIndices[2]];
				int num4 = currentArray[permIndices[3]];
				T index = permutations[0] * num1;
				if (num2 > num1) {
					index += permutations[1] * (num2 - 1);
					if (num3 > num2) { // num1 < num2 < num3
						index += permutations[2] * (num3 - 2);
					}
					else if (num3 > num1) { // num1 < num3 < num2
						index += permutations[2] * (num3 - 1);
					}
					else { // num3 < num1 < num2
						index += permutations[2] * num3;
					}
				}
				else { 
					index += permutations[1] * num2;
					if (num3 < num2) { // num3 < num2 < num1
						index + permutations[2] * num3;
					}
					else if (num3 < num1) { // num2 < num3 < num1
						index + permutations[2] * (num3 - 1);
					}
					else { // num2 < num1 < num3
						index + permutations[2] * (num3 - 2);
					}
				}
			}
			*/
//			T index = permutations[0] * currentArray[permIndices[0]];
//			return index;
		}
		void printPermIndices() const {
			for (int i = 0; i < K; ++i) {
				cout << permIndices[i] << " ";
			}
			cout << endl;
		}
		void printcurrentPerm() const {
			for (int i = 0; i < K; ++i) {
				cout << currentArray[permIndices[i]] << " ";
			}
			cout << endl;
		}
		std::string DebugStringOfCurrentPermutation() const {
			std::stringstream ss;
			ss << "( ";
			for (int i = 0; i < K; ++i) {
				ss << currentArray[permIndices[i]] << " ";
			}
			ss << ")";
			return ss.str();
		}
		int &operator[](int i) {
			return permIndices[i];
		}
		inline int at(int i) const {
			return currentArray[permIndices[i]];
		}
};
#endif // PERMUTATION_ITERATOR_H_