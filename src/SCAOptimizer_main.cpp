#include "SCAoptimizer.h"

#include <climits>
#include <iostream>
#include <string>

using namespace std;

int main(int argc, const char * argv[]) {
	std::string filename = argv[1];
	int N, V, K;
	const bool is_box4_optimization = argc > 2 ? true : false;
	
	ifstream myfile;
	myfile.open(filename.c_str());

	if ( ! myfile.is_open() ) {
		std::cout << "Can't open file: " << filename << std::endl;
		return 0;
	}

	myfile >> N;
	myfile >> K;
	myfile >> V;
	myfile.close();
	
	SCAoptimizer<long> optimizer (N, K, V, filename);

	optimizer.ReadInputFromFile(filename);

	if (is_box4_optimization) {
		std::cout << "Is BOX4: " << (optimizer.CheckBox() ? "Yes!" : "NO") << std::endl;
	}
	else {
		std::cout << "Is SCA: " << (optimizer.checkSCA() ? "Yes!" : "NO") << std::endl;
//		std::cout << "Is SPAN: " << (optimizer.CheckType() ? "Yes!" : "NO") << std::endl;
	}

	int iterations_since_last_improvement = 0;
	int times_tried_new_fixed_array_since_last_improvement = 0;
	int num_perms_last_row_covers = 0;
	int prev_num_perms_last_row_covered = 0;
	const int kMaxNumAttempts = 50000; // arbitrary. Make some function of K & V
	const int kTimesUntilTryNewLastPermutation = 300; // arbitrary

	while (times_tried_new_fixed_array_since_last_improvement < kMaxNumAttempts) {
		prev_num_perms_last_row_covered = num_perms_last_row_covers;
		num_perms_last_row_covers = optimizer.DoOneOptimizationPass(is_box4_optimization);
		if (num_perms_last_row_covers == 0) {
			std::stringstream output_file_name;
			output_file_name << filename << "-output(" << optimizer.SizeOfSCA() << ")";
			optimizer.WriteSCAToFile(output_file_name.str(), true);
			iterations_since_last_improvement = 0;
			times_tried_new_fixed_array_since_last_improvement = 0;
		}
		else if (prev_num_perms_last_row_covered != num_perms_last_row_covers) {
			// It improved
			iterations_since_last_improvement = 0;
		}
		else {
			++iterations_since_last_improvement;
		}

		if (iterations_since_last_improvement > kTimesUntilTryNewLastPermutation) {
			optimizer.shuffleAllArrays();
			++times_tried_new_fixed_array_since_last_improvement;
			iterations_since_last_improvement = 0;
		}
	}

	if (optimizer.SizeOfSCA() < N) {
		std::cout << "Removed some arrays!" << std::endl;
		return 0;
	} 
	else {
		std::cout << "Could not remove any arrays!" << std::endl;
		return 1;
	}	
}