#ifndef SCAOPTIMIZER_H
#define SCAOPTIMIZER_H

#include "Array2D.h"
#include "Box4.h"
#include "CoveringType.h"
#include "DiGraph.h"
#include "PermutationIterator.h"

#include <algorithm>
#include <assert.h>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

template<typename T>
class SCAoptimizer {
	private:		
		const int kNumElements;
		const int kStrength;
		
		int current_number_of_permutations_;
		const int kOriginalNumberOfPermutations;

		const std::string FILENAME;
		std::stringstream info_about_current_run_;

		unsigned int current_run_number_;

		int * shuffled_array_index_to_true_index_;
		Array2D<int> permutation_array_;

		unsigned int seed;

		unsigned int * permutation_index_table_;

		DiGraph constraints_;
		bool has_constraints_;

		Array2D<bool> avoids_;
		bool has_avoids_;

		void ResetTables();
		void ShuffleArrays();
		int NumPermutationsCoveredByLastRow() const;
		int BoxesCoveredByLastRow() const;
		int TypesCoveredByLastRow() const;
		void RemoveLastPermutation() { --current_number_of_permutations_; }

		PermutationIterator<T> GetPermutationIterator() const { 
			return PermutationIterator<T>(kNumElements, kStrength); }
public:		
		SCAoptimizer(int N, int strength, int numElements, std::string file) :
			kNumElements (numElements), kStrength (strength),
			current_number_of_permutations_(N), kOriginalNumberOfPermutations(N),
			FILENAME(file), current_run_number_(0),
			permutation_array_(N, numElements),
			seed (std::time(NULL)), constraints_(kNumElements), has_constraints_(false),
			avoids_(kNumElements, kNumElements), has_avoids_(false) {
			info_about_current_run_ << "Random seed: " << seed << std::endl;
			time_t now = time(0);
			char* dt = ctime(&now);
			info_about_current_run_ << "Began with " << N << " arrays at " << dt;

			// set permutation_index_table_ to size of largest permutation index
			permutation_index_table_ = new unsigned int[
				PermutationIterator<T>::getSizeOfLargestPermutationIndex(kNumElements,kStrength)];
			shuffled_array_index_to_true_index_ = new int[N];
			for (int i = 0; i < N; ++i) {
				shuffled_array_index_to_true_index_[i] = i;
			}
		}
		~SCAoptimizer() {
			delete [] permutation_index_table_;
			delete [] shuffled_array_index_to_true_index_;
		}
		void shuffleAllArrays();
		void ReadInputFromFile(std::string filename);
		void ChangeAllPermutationsViaLinearExtensions();
		void UpdatePermutationsAndStillCoverBox4s();
		int DoOneOptimizationPass(bool is_box4_optimization);
		
		bool WriteSCAToFile(std::string output_name, bool include_run_info) const;
		void WriteSCAToString(bool include_run_info, std::string* output_string) const;
		bool checkSCA() const;
		bool CheckBox() const;
		bool CheckType() const;
		void printSCA() const;
		int SizeOfSCA() const { return current_number_of_permutations_; }
		bool printMissingPerms() const;
		void printArrayOrder() const;

		void setSeed(unsigned int);
		static T NpermK(int N, int k);
		static T NtoK(int N, int k);
		static void indexToPerm(T index, int N, int k);
};

template<typename T>
void SCAoptimizer<T>::setSeed(unsigned int x) {
	seed = x;
}

template<typename T>
void SCAoptimizer<T>::ResetTables() {
	T limit = NtoK(kNumElements, kStrength);
	for (T i = 0; i < limit; ++i) {
		permutation_index_table_[i] = 0;
	}
	current_run_number_ = 1;
}

template<typename T>
bool SCAoptimizer<T>::WriteSCAToFile(
		std::string output_name, const bool include_run_info) const {
	std::string output;
	WriteSCAToString(include_run_info, &output);
	std::ofstream output_file;
	output_file.open(output_name.c_str());
	if (!output_file.is_open()) {
		return false;
	}

	output_file << output;

	output_file.close();
	return true;
}

template<typename T>
void SCAoptimizer<T>::WriteSCAToString(
		const bool include_run_info, std::string* output_string) const{
	std::stringstream ss;

	ss << SizeOfSCA() << " " << kStrength << " " << kNumElements << std::endl;

	// Constraints
	if (has_constraints_) {
		for (int v = 0; v < kNumElements; ++v) {
			for (int u = 0; u < kNumElements; ++u) {
				if (constraints_.HasEdge(v, u)) {
					ss << "Con " << v << " " << u << std::endl;
				}
			}
		}
	}

	// Avoids
	if (has_avoids_) {
		for (int v = 0; v < kNumElements; ++v) {
			for (int u = 0; u < kNumElements; ++u) {
				if (avoids_(v, u) == true) {
					ss << "Avd " << v << " " << u << std::endl;
				}
			}
		}
	}

	int arrayNum;
	for (int i = 0; i < SizeOfSCA(); ++i) {
		arrayNum = shuffled_array_index_to_true_index_[i];
		for (int j = 0; j < kNumElements; ++j) {
			ss << permutation_array_(arrayNum, j) << " ";
		}
		ss << std::endl;
	}
	if (include_run_info) {
		ss << info_about_current_run_.str();
	}
	*output_string = ss.str();
}

template<typename T>
void SCAoptimizer<T>::ReadInputFromFile(std::string filename) {
	std::ifstream myfile;
	int N, k, v;
	permutation_array_(0,0) = 1 + 1;
	myfile.open(filename.c_str());
	if (!myfile.is_open()) {
		std::cout << "Incorrect file name:" << filename << std::endl;
		return;
	}

	myfile >> N;
	myfile >> k;
	myfile >> v;

	assert(SizeOfSCA() == N);
	assert(kNumElements == v);
	assert(kStrength == k);
	
	std::string irrelevant;
	int pre_constraint;
	int post_constraint;
	std::getline(myfile, irrelevant);
	int nextChar = myfile.peek();
	while (myfile.peek() == 'C' || myfile.peek() == 'A') {
		if (myfile.peek() == 'C') {
			std::getline(myfile, irrelevant, ' ');
			myfile >> pre_constraint;
			myfile >> post_constraint;
			constraints_.AddEdge(pre_constraint, post_constraint);
			has_constraints_ = true;
		}
		else if (myfile.peek() == 'A') {
			std::getline(myfile, irrelevant, ' ');
			myfile >> pre_constraint;
			myfile >> post_constraint;
			avoids_(pre_constraint, post_constraint) = true;
			has_avoids_ = true;
		}
		std::getline(myfile, irrelevant); // flush out newline
	}
	

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < kNumElements; ++j) {
			myfile >> permutation_array_(i, j);
		}
	}

	myfile.close();
}

// Runs one pass of the optimization. Returns the number of sub-permutations 
// that the last permutation uniquely covered. If it was 0, then a row was removed
template<typename T>
int SCAoptimizer<T>::DoOneOptimizationPass(const bool is_box_4_optimization) {
	int numLastPermCovers;
	if (is_box_4_optimization) {
		UpdatePermutationsAndStillCoverBox4s();
		numLastPermCovers = BoxesCoveredByLastRow();
	}
	else {
		ChangeAllPermutationsViaLinearExtensions();
		numLastPermCovers = NumPermutationsCoveredByLastRow();
//		numLastPermCovers = TypesCoveredByLastRow();
	}
	if (numLastPermCovers == 0) {
		RemoveLastPermutation();
		time_t now = time(0);
		char* dt = ctime(&now);
		info_about_current_run_ << "Reached " << SizeOfSCA() << " at time " << dt;
		std::cout << "Deleted Array!! Down to " << SizeOfSCA() << " from "
			<< kOriginalNumberOfPermutations << "! - " << dt;
	}
	ShuffleArrays();
	return numLastPermCovers;
}

template<typename T>
void SCAoptimizer<T>::ChangeAllPermutationsViaLinearExtensions() {
	++current_run_number_;
	if (current_run_number_ == 0) {
		ResetTables();
	}
	// Do we need these static? Perhaps do some performance testing to see
	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
//	PartialOrder partial_order(kNumElements);
	int array_num;
	for (int array_index = 0; array_index < SizeOfSCA() - 1; ++array_index) {
		DiGraph partial_order(kNumElements);
		if (has_constraints_) {
			partial_order.MergeDiGraphs(constraints_, false);
		}
		array_num = shuffled_array_index_to_true_index_[array_index];
		permutation_iterator.setArray(permutation_array_[array_num]);
		permutation_iterator.resetPerm();
		do {
			int prev_perm_value;
			int current_perm_value = permutation_iterator.at(0);
			T permutation_index = permutation_iterator.getIndexOfPermutation();
			// Use below instead of above for CROSS/SPAN
//			T permutation_index = CoveringType(permutation_iterator).GetIndex(kNumElements);
			if (permutation_index_table_[permutation_index] != current_run_number_) {
				if (has_avoids_) {
					// Check if this is something in avoids (as in, we don't care about covering it)
					bool permutation_is_not_required_for_coverage = false;
					for (int i = 0; i < kStrength; ++i) {
						for (int j = i + 1; j < kStrength; ++j) {
							permutation_is_not_required_for_coverage |=
								avoids_(permutation_iterator.at(i), permutation_iterator.at(j));
						}
					}
					if (permutation_is_not_required_for_coverage) {
						continue;
					}
				}

				permutation_index_table_[permutation_index] = current_run_number_;
				// Below corresponds to CROSS
//				partial_order.AddEdge(permutation_iterator.at(0), permutation_iterator.at(1));
//				partial_order.AddEdge(permutation_iterator.at(1), permutation_iterator.at(2));
//				partial_order.AddEdge(permutation_iterator.at(2), permutation_iterator.at(3));
				// Below corresponds to SPAN
//				partial_order.AddEdge(permutation_iterator.at(0), permutation_iterator.at(1));
	//			partial_order.AddEdge(permutation_iterator.at(0), permutation_iterator.at(2));
		//		partial_order.AddEdge(permutation_iterator.at(1), permutation_iterator.at(3));
			//	partial_order.AddEdge(permutation_iterator.at(2), permutation_iterator.at(3));
				
				// Below corresponds to SCA
				for (int j = 1; j < kStrength; ++j) {
					prev_perm_value = current_perm_value;
					current_perm_value = permutation_iterator.at(j);
					partial_order.AddEdge(prev_perm_value, current_perm_value);
				}	
			}
		} while (permutation_iterator.nextPerm());

		std::vector<int> new_total_order;
		bool is_valid_total_order = partial_order.ExtendToTotalOrder(&new_total_order);
		// If this is false, the extension failed so we made an incorrect partial order.
		assert(is_valid_total_order);
		for (int i = 0; i < kNumElements; ++i) {
			permutation_array_(array_num, i) = new_total_order[i];
		}
	}
}

template<typename T>
void SCAoptimizer<T>::UpdatePermutationsAndStillCoverBox4s() {
	++current_run_number_;
	if (current_run_number_ == 0) {
		ResetTables();
	}
	// Do we need these static? Perhaps do some performance testing to see
	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
	//	PartialOrder partial_order(kNumElements);
	int array_num;
	for (int array_index = 0; array_index < SizeOfSCA() - 1; ++array_index) {
		std::vector<DiGraph*> list_of_merged_boxes;
		std::vector<Box4> DEBUG_list_all_boxes;
//		DiGraph partial_order(kNumElements);
		array_num = shuffled_array_index_to_true_index_[array_index];
		permutation_iterator.setArray(permutation_array_[array_num]);
		permutation_iterator.resetPerm();
		do {
			DiGraph* previous_graph_merged = nullptr;
			Box4 box(permutation_iterator);
			T permutation_index = box.GetIndex(kNumElements);
			if (permutation_index_table_[permutation_index] == current_run_number_) {
				continue;
			}
			DEBUG_list_all_boxes.push_back(box);
			permutation_index_table_[permutation_index] = current_run_number_;
			unsigned int index = 0;
			while (index < list_of_merged_boxes.size()) {
				DiGraph* partial_order = list_of_merged_boxes[index];
				int correlated_with_box = partial_order->CorrelatedWithBox(box);
				if (!!correlated_with_box) {
					if (previous_graph_merged == nullptr) {
						// reverse if it is anticorrelated
						partial_order->AddBoxRelation(box, 
							(correlated_with_box == 1) ? false : true);
						previous_graph_merged = partial_order;
						++index;
					}
					else {
						// reverse if it is anticorrelated
						previous_graph_merged->MergeDiGraphs(*partial_order,
							(correlated_with_box == 1) ? false : true);
						// Deleting partial order we just merged
						delete partial_order;
						list_of_merged_boxes[index] = list_of_merged_boxes.back();
						list_of_merged_boxes.pop_back();
					}
				}
				else {
					++index;
				}
			}
			// we never found a partial order our box was correlated with,
			// so make a new partial order
			if (previous_graph_merged == nullptr) {
				DiGraph* new_partial_order = new DiGraph(kNumElements);
				new_partial_order->AddBoxRelation(box, false);
				list_of_merged_boxes.push_back(new_partial_order);
			}
		} while (permutation_iterator.nextPerm()); // iterating through permutation

//		std::cout << "Num uncor posets: " << list_of_merged_boxes.size() << std::endl;
		// No boxes covered, so row is already ``useless''
		if (list_of_merged_boxes.size() == 0) {
			return;
		}

		DiGraph* final_partial_order = list_of_merged_boxes[0];

		for (unsigned int i = 1; i < list_of_merged_boxes.size(); ++i) {
			// URGENT! Cannot simply randomly merge them; each merge potentially
			// forces other merges!!. Best strategy? Biggest with rev(biggest)? Then random?
			bool reverse_graph = ((std::rand() % 2) == 0);
			if (list_of_merged_boxes.size() == 2) {
				final_partial_order->MergeDiGraphs(*list_of_merged_boxes[i], true);
			}
			else {
				final_partial_order->MergeDiGraphs(*list_of_merged_boxes[i], false);
			}
//			delete list_of_merged_boxes[i];
		}

		std::vector<int> new_total_order;
		bool is_valid_total_order = final_partial_order->ExtendToTotalOrder(&new_total_order);
//		delete final_partial_order;
		// DEBUG
		if (!is_valid_total_order) {
			std::cout << "Num partial orders: " << list_of_merged_boxes.size() << std::endl;
			/*
			for (auto p_order : list_of_merged_boxes) {
				p_order->PrintGraph();
			}
			for (int i = 0; i < kNumElements; ++i) {
				std::cout << permutation_array_(array_num, i) << " ";
			}
			std::cout << std::endl;
			*/
			std::vector<int> boxes;
			for (auto box : DEBUG_list_all_boxes) {
				box.GetNumbersForDebugging(&boxes);
				for (auto i : boxes) {
					std::cout << i << " ";
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
			for (unsigned int i = 0; i < list_of_merged_boxes.size(); ++i) {
				list_of_merged_boxes[i]->PrintGraph();
			}
			
			abort();
		} // END DEBUG
		// If this is false, the extension failed so we made an incorrect partial order.
		assert(is_valid_total_order);
		for (int i = 0; i < kNumElements; ++i) {
			permutation_array_(array_num, i) = new_total_order[i];
		}

		// DEBUG
		for (unsigned int i = 1; i < list_of_merged_boxes.size(); ++i) {
			delete list_of_merged_boxes[i];
		}
	}
}
template<typename T>
bool SCAoptimizer<T>::checkSCA() const {
	T limit = NpermK(kNumElements, kStrength);
	std::vector<bool> permutations_found(limit, false);
	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
	for (int array_num = 0; array_num < SizeOfSCA(); ++array_num) {
		int shuffled_array_num = shuffled_array_index_to_true_index_[array_num];
		permutation_iterator.setArray(permutation_array_[shuffled_array_num]);
		permutation_iterator.resetPerm();
		do {
			permutations_found[permutation_iterator.getPermutationIndex()] = true;
		} while (permutation_iterator.nextPerm());
	}
	std::vector<int> permutation;
	for (T i = 0; i < limit; ++i) {
		bool permutation_can_be_avoided = false;
		bool permutation_violates_constraint = false;
		// Compute this subpermutation
		permutation_iterator.GetPermutationFromIndex(i, &permutation);
		if (has_constraints_ || has_avoids_) {
			// Check to see if this missed permutation is ignored because of
			// "avoids" or prohibited due to "constraints"
			for (int j = 0; j < kStrength; ++j) {
				for (int k = j + 1; k < kStrength; ++k) {
					if (has_avoids_) {
						permutation_can_be_avoided |= avoids_(permutation[j], permutation[k]);
					}
					if (has_constraints_) {
						// path from 2nd to 1st in constraints graph means that this violates a constraint
						permutation_violates_constraint |= constraints_.HasDirectedPath(
							permutation[k], permutation[k], permutation[j], permutation[j]);
					}
				}
			}
		}
		if (!permutations_found[i] && !permutation_violates_constraint && !permutation_can_be_avoided) {
			std::cout << "Failed to cover:";
			for (int ii = 0; ii < kStrength; ++ii) {
				std::cout << " " << permutation[ii];
			}
			std::cout << std::endl;
			return false;
		}
		if (permutations_found[i] && permutation_violates_constraint) {
			std::cout << "\nPermutation found that violates constraint:" << std::endl;
			for (int ii = 0; ii < kStrength; ++ii) {
				std::cout << " " << permutation[ii];
			}
			std::cout << std::endl;
			return false;
		}
	}
	return true;
}

template<typename T>
bool SCAoptimizer<T>::CheckBox() const {
	T limit = NtoK(kNumElements, kStrength);
	std::vector<bool> permutations_found(limit, true);
	for (int a = 0; a < kNumElements; ++a) {
		for (int b = 0; b < kNumElements; ++b) {
			if (b == a) { continue; }
			for (int c = 0; c < kNumElements; ++c) {
				if (c == a || c == b) { continue; }
				for (int d = 0; d < kNumElements; ++d) {
					if (d == a || d == b || d == c) { continue; }
					Box4 box(a, b, c, d);
					permutations_found[box.GetIndex(kNumElements)] = false;
				}
			}
		}
	}

	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
	for (int array_num = 0; array_num < SizeOfSCA(); ++array_num) {
		int shuffled_array_num = shuffled_array_index_to_true_index_[array_num];
		permutation_iterator.setArray(permutation_array_[shuffled_array_num]);
		permutation_iterator.resetPerm();
		do {
			permutations_found[Box4(permutation_iterator).GetIndex(kNumElements)] = true;
		} while (permutation_iterator.nextPerm());
	}
	for (T i = 0; i < limit; ++i) {
		if (!permutations_found[i]) {
			return false;
		}
	}
	return true;
}

template<typename T>
bool SCAoptimizer<T>::CheckType() const {
	T limit = NtoK(kNumElements, kStrength);
	std::vector<bool> permutations_found(limit, true);
	for (int a = 0; a < kNumElements; ++a) {
		for (int b = 0; b < kNumElements; ++b) {
			if (b == a) { continue; }
			for (int c = 0; c < kNumElements; ++c) {
				if (c == a || c == b) { continue; }
				for (int d = 0; d < kNumElements; ++d) {
					if (d == a || d == b || d == c) { continue; }
					CoveringType sub_permutation(a, b, c, d);
					permutations_found[sub_permutation.GetIndex(kNumElements)] = false;
				}
			}
		}
	}

	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
	for (int array_num = 0; array_num < SizeOfSCA(); ++array_num) {
		int shuffled_array_num = shuffled_array_index_to_true_index_[array_num];
		permutation_iterator.setArray(permutation_array_[shuffled_array_num]);
		permutation_iterator.resetPerm();
		do {
			permutations_found[CoveringType(permutation_iterator).GetIndex(kNumElements)] = true;
		} while (permutation_iterator.nextPerm());
	}
	for (T i = 0; i < limit; ++i) {
		if (!permutations_found[i]) {
			return false;
		}
	}
	return true;
}

template<typename T>
bool SCAoptimizer<T>::printMissingPerms() const {
	markUsed();
	for (T i = 0; i < NpermK(kNumElements, kStrength); ++i) {
		if (permutation_index_table_[i] != current_run_number_) {
			std::cout << "i = " << i << ", current_run_number_ = " << current_run_number_
				<< ", permTab = " << permutation_index_table_[i] << ": ";
			indexToPerm(i, kNumElements, kStrength);
			return false;
		}
	}
	return true;
}

template<typename T>
void SCAoptimizer<T>::printSCA() const {
	int arrayNum;
	for (int i = 0; i < SizeOfSCA(); ++i) {
		arrayNum = shuffled_array_index_to_true_index_[i];
		for (int j = 0; j < kNumElements; ++j) {
			std::cout << permutation_array_(arrayNum, j) << " ";
		}
		std::cout << std::endl;
	}
}

template<typename T>
int SCAoptimizer<T>::NumPermutationsCoveredByLastRow() const {
	int numCovered = 0;

	int arrayNum = shuffled_array_index_to_true_index_[SizeOfSCA() - 1];
	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
	permutation_iterator.setArray(permutation_array_[arrayNum]);
	permutation_iterator.resetPerm();

	do {
		if (permutation_index_table_[permutation_iterator.getIndexOfPermutation()]
			!= current_run_number_) {
			++numCovered;
		}
	} while (permutation_iterator.nextPerm());
	return numCovered;
}

template<typename T>
int SCAoptimizer<T>::TypesCoveredByLastRow() const {
	int numCovered = 0;

	int arrayNum = shuffled_array_index_to_true_index_[SizeOfSCA() - 1];
	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
	permutation_iterator.setArray(permutation_array_[arrayNum]);
	permutation_iterator.resetPerm();

	do {
		CoveringType sub_permutation(permutation_iterator);
		if (permutation_index_table_[sub_permutation.GetIndex(kNumElements)] != current_run_number_)
			++numCovered;
	} while (permutation_iterator.nextPerm());
	return numCovered;
}


template<typename T>
int SCAoptimizer<T>::BoxesCoveredByLastRow() const {
	int numCovered = 0;

	int arrayNum = shuffled_array_index_to_true_index_[SizeOfSCA() - 1];
	PermutationIterator<T> permutation_iterator = GetPermutationIterator();
	permutation_iterator.setArray(permutation_array_[arrayNum]);
	permutation_iterator.resetPerm();

	do {
		Box4 box(permutation_iterator);
		if (permutation_index_table_[box.GetIndex(kNumElements)] != current_run_number_)
			++numCovered;
	} while (permutation_iterator.nextPerm());
	return numCovered;
}

template<typename T>
void SCAoptimizer<T>::ShuffleArrays() {
	std::random_shuffle(&shuffled_array_index_to_true_index_[0],
		&shuffled_array_index_to_true_index_[SizeOfSCA() - 1]);
}

template<typename T>
void SCAoptimizer<T>::shuffleAllArrays() {
	std::random_shuffle(&shuffled_array_index_to_true_index_[0],
		&shuffled_array_index_to_true_index_[SizeOfSCA()]);
}

template<typename T>
void SCAoptimizer<T>::printArrayOrder() const {
	for (int i = 0; i < SizeOfSCA(); ++i) {
		std::cout << shuffled_array_index_to_true_index_[i] << " ";
	}
	std::cout << std::endl;
}

template<typename T>
void SCAoptimizer<T>::indexToPerm(T index, int N, int k) {
	int * arr = new int[k];
	int currentBase = N - k + 1;
	for (int currIndex = k - 1; currIndex >= 0; --currIndex) {
		arr[currIndex] = index % currentBase;
		index = index / currentBase;
		++currentBase;
	}
	for (int i = k; i >= 0; --i) {
		for (int j = i - 1; j >= 0; --j) {
			if (arr[j] <= arr[i])
				++arr[i];
		}
	}
	for (int i = 0; i < k; ++i) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;

	delete[] arr;
}

template<typename T>
T SCAoptimizer<T>::NpermK(int N, int k) {
	T result = N;
	for (int i = N - 1; i > N - k; --i) {
		result *= i;
	}
	return result;
}

template<typename T>
T SCAoptimizer<T>::NtoK(int N, int k) {
	T result = N;
	for (int i = 1; i < k; ++i) {
		result *= N;
	}
	return result;
}

#endif